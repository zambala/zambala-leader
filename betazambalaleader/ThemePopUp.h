//
//  ThemePopUp.h
//  betazambalaleader
//
//  Created by zenwise mac 2 on 3/27/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThemePopUp : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *lightButton;
@property (strong, nonatomic) IBOutlet UIButton *darkButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
- (IBAction)onCancelButtonTap:(id)sender;
- (IBAction)onOkButtonTap:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *okButton;
@end
