//
//  ClosingAdvicePopUp.m
//  betazambalaleader
//
//  Created by zenwise mac 2 on 3/24/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "ClosingAdvicePopUp.h"
#import "UIViewController+MJPopupViewController.h"


@interface ClosingAdvicePopUp ()

@end

@implementation ClosingAdvicePopUp

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view1.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.view1.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.view1.layer.shadowOpacity = 1.0f;
    self.view1.layer.shadowRadius = 2.1f;
   // self.view1.layer.cornerRadius=1.0f;
    self.view1.layer.masksToBounds = NO;
    
    
    
    self.closeAdviceButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.closeAdviceButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.closeAdviceButton.layer.shadowOpacity = 1.0f;
    self.closeAdviceButton.layer.shadowRadius = 1.0f;
    self.closeAdviceButton.layer.cornerRadius=2.1f;
    self.closeAdviceButton.layer.masksToBounds = NO;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelButtonTap:(id)sender {
    
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}
@end
