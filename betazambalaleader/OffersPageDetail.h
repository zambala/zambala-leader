//
//  ViewController1.h
//  OfferPage
//
//  Created by guna on 03/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersPageDetail : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *listTableView;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)onBackButtonTap:(id)sender;

@end
