//
//  OTPVerificationViewViewController.h
//  testing
//
//  Created by zenwise technologies on 23/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPVerificationViewViewController : UIViewController<UITextFieldDelegate>
- (IBAction)verifyAction:(id)sender;
- (IBAction)resendAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *OTPText;
@property NSString *OTPString;
@property NSMutableDictionary* responseDictonary;

@end
