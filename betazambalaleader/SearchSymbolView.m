//
//  SearchSymbolView.m
//  testing
//
//  Created by zenwise technologies on 16/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "SearchSymbolView.h"
#import "AppDelegate.h"
#import "HomePage.h"
#import "PSWebSocket.h"
#import "SearchTableViewCell.h"
#import <UITextField_AutoSuggestion/UITextField+AutoSuggestion.h>

@interface SearchSymbolView ()<UITextFieldAutoSuggestionDataSource, UITextFieldDelegate>
{
    NSString * exchangeString;
    NSString * instrumentTypeStr;
    AppDelegate * delegate1;
    NSString * equityStr;
    NSInteger objectAtIndex;
    NSMutableArray * expiry;
    NSMutableArray * strike;
}

@end

#define Symbol_ID @"symbol_id"
#define WEEKS @[@"Monday", @"Tuesday", @"Wednesday", @"Thirsday", @"Friday", @"Saturday", @"Sunday"]


@implementation SearchSymbolView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //appdelegate//
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.company=[[NSMutableArray alloc]init];
    self.symbols=[[NSMutableArray alloc]init];
    self.symbolTxt.delegate=self;
    exchangeString=@"BSE";
    self.commodityBtn.enabled=NO;
    self.responseArray=[[NSMutableArray alloc]init];
     expiry=[[NSMutableArray alloc]init];
   strike=[[NSMutableArray alloc]init];

    
    [self.exchangeSegment addTarget:self action:@selector(selectedSegment) forControlEvents:UIControlEventValueChanged];
    
    [self setBIDRadioBtn];
    
    [self.equityBtn addTarget:self action:@selector(equityAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.derivativeBtn addTarget:self action:@selector(derivativeAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.currencyBtn addTarget:self action:@selector(currencyAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.commodityBtn addTarget:self action:@selector(commodityAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.futureBtn addTarget:self action:@selector(futureAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.optionBtn addTarget:self action:@selector(optionAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.CE addTarget:self action:@selector(ceAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.PEBtn addTarget:self action:@selector(peAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.scroll.contentSize=CGSizeMake(375, 800);
    
    instrumentTypeStr=@"EQ";
    exchangeString=@"BSE";
    
    [self equityAction];
    
    self.symbolTxt.delegate=self;
    self.searchTableView.hidden=YES;
    
    self.symbolTxt.delegate = self;
    self.symbolTxt.autoSuggestionDataSource = self;
    self.symbolTxt.fieldIdentifier = Symbol_ID;
    [self.symbolTxt observeTextFieldChanges];
    
}

- (void)loadWeekDays {
    // cancel previous requests
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadWeekDaysInBackground) object:self];
    [self.symbolTxt setLoading:false];
    
    // clear previous results
    [self.symbols removeAllObjects];
    [self.symbolTxt reloadContents];
    
    // start loading
    [self.symbolTxt setLoading:true];
    [self performSelector:@selector(loadWeekDaysInBackground) withObject:self afterDelay:2.0f];
}

- (void)loadWeekDaysInBackground {
    // finish loading
    [self.symbolTxt setLoading:false];
    
    
    [self.symbols addObjectsFromArray:self.company];
   
      [self.symbolTxt reloadContents];
    
}


//    [self.symbols addObjectsFromArray:WEEKS];




//segment method//

-(void)selectedSegment
{
    if(self.exchangeSegment.selectedSegmentIndex==0)
    {
        exchangeString=@"BSE";
        self.commodityBtn.enabled=NO;
        self.equityBtn.enabled=YES;
        self.equityBtn.selected=YES;
        self.derivativeBtn.enabled=YES;
        self.currencyBtn.selected=NO;
        self.derivativeBtn.selected=NO;
        self.currencyBtn.selected=NO;
        [self equityAction];
    }
    else if (self.exchangeSegment.selectedSegmentIndex==1)
    {
        exchangeString=@"NSE";
        self.commodityBtn.enabled=NO;
        self.equityBtn.enabled=YES;
        self.derivativeBtn.enabled=YES;
        self.currencyBtn.selected=NO;
        self.equityBtn.selected=YES;
        self.derivativeBtn.selected=NO;
        self.currencyBtn.selected=NO;
        [self equityAction];
    }
    
    else if (self.exchangeSegment.selectedSegmentIndex==2)
    {
        exchangeString=@"MCX";
        self.commodityBtn.enabled=YES;
        self.currencyBtn.selected=YES;
        self.equityBtn.enabled=NO;
        self.derivativeBtn.enabled=NO;
        [self currencyAction];
        self.PEBtn.selected=NO;
         equityStr=@"FUT";
        [self futureAction];
       
        
    }
}



//RADIO BUTTON//

-(void)setBIDRadioBtn
{
    [self.equityBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.equityBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.equityBtn.selected = NO;
    
    [self.derivativeBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.derivativeBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.derivativeBtn.selected = NO;
    
    [self.currencyBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.currencyBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.currencyBtn.selected = NO;
    
    [self.commodityBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.commodityBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.commodityBtn.selected = NO;
    
    [self.futureBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.futureBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.futureBtn.selected = NO;
    
    [self.optionBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.optionBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.optionBtn.selected = NO;
    
    [self.CE setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.CE setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.CE.selected = NO;
    
    [self.PEBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.PEBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.PEBtn.selected = NO;
    
}

//button methods//

-(void)equityAction
{
    self.equityBtn.selected = YES;
    self.derivativeBtn.selected = NO;
    self.currencyBtn.selected = NO;
    self.commodityBtn.selected = NO;
    self.instrumentLbl.hidden=NO;
    self.instrumentHgt.constant=0;
    self.instrumentTxtHgt.constant=0;
    self.instrumentLblTop.constant=0;
    self.instrumentViewTop.constant=0;
    self.instrumentView.hidden=YES;
    self.optionLbl.hidden=YES;
    self.optionLblHgt.constant=0;
     self.optionViewHgt.constant=0;
    self.optionLblTop.constant=0;
    self.optionViewTop.constant=0;
    self.optionView.hidden=YES;
    self.expiryDateLbl.hidden=YES;
    self.expiryLblHgt.constant=0;
     self.expiryTxtHgt.constant=0;
    self.expiryDateLblTop.constant=0;
    self.expiryDateTFTop.constant=0;
    self.expiryDateTxt.hidden=YES;
    self.strikePrice.hidden=YES;
    self.strikeHgt.constant=0;
    self.strikeTxtHgt.constant=0;
    self.strikePricelblTop.constant=0;
    self.strikePriceTFTop.constant=0;
    self.strikePriceTxt.hidden=YES;
    self.expHeightContraint.constant=0;
    self.strikeHeightConstraint.constant=0;
    self.lineViewHeightConstraint.constant=0;
    self.imageViewHeightContraint.constant=0;
    self.spLineViewHeightConstraint.constant=0;
    self.spImageViewHeightConstraint.constant=0;
    self.TFHeightConstraint.constant=0;
    self.spTFHeightConstraint.constant=0;
    self.symbolTxt.text=@"";
    self.expiryDateTxt.text=@"";
    self.strikePriceTxt.text=@"";
    if(self.exchangeSegment.selectedSegmentIndex==0)
    {
        exchangeString=@"BSE";
    }
    
    else if(self.exchangeSegment.selectedSegmentIndex==1)
    {
        exchangeString=@"NSE";
    }
    
    equityStr=@"EQ";

    
}

-(void)derivativeAction
{
    self.equityBtn.selected = NO;
    self.derivativeBtn.selected = YES;
    self.currencyBtn.selected = NO;
    self.commodityBtn.selected = NO;
    self.instrumentLbl.hidden=NO;
    self.instrumentView.hidden=NO;
    self.futureBtn.selected = YES;
    self.instrumentHgt.constant=14;
    self.instrumentTxtHgt.constant=63.2;
    self.instrumentLblTop.constant=24;
    self.instrumentViewTop.constant=10;
    self.optionLbl.hidden=YES;
    self.optionLblHgt.constant=0;
    self.optionViewHgt.constant=0;
    self.optionLblTop.constant=0;
    self.optionViewTop.constant=0;
    self.optionView.hidden=YES;
    self.expiryDateLbl.hidden=NO;
    self.expiryLblHgt.constant=14;
    self.expiryTxtHgt.constant=30;
    self.expiryDateLblTop.constant=24;
    self.expiryDateTFTop.constant=10;
    self.expiryDateTxt.hidden=NO;
   // self.strikePrice.hidden=YES;
    //self.strikeHgt.constant=0;
    //self.strikeTxtHgt.constant=0;
    //self.strikePricelblTop.constant=0;
    //self.strikePriceTFTop.constant=0;
    //self.strikePriceTxt.hidden=YES;
    self.expHeightContraint.constant=63.4;
    self.strikeHeightConstraint.constant=63.4;
    self.lineViewHeightConstraint.constant=1;
    self.spLineViewHeightConstraint.constant=1;
    self.imageViewHeightContraint.constant=30;
    self.spImageViewHeightConstraint.constant=30;
    self.TFHeightConstraint.constant=17;
    self.spTFHeightConstraint.constant=17;
    self.symbolTxt.text=@"";
    self.expiryDateTxt.text=@"";
    self.strikePriceTxt.text=@"";
    
    self.strikeView.hidden=YES;
    self.strikeDropDown.hidden=YES;
    
     self.strikePricelblTop.constant=14;
    self.futureBtn.selected=YES;
    
    self.strikePrice.hidden=NO;
    self.strikeHgt.constant=0;
    self.strikeTxtHgt.constant=0;
    self.strikePricelblTop.constant=0;
    self.strikePriceTFTop.constant=14;
    self.strikePriceTxt.hidden=YES;
    self.strikeHeightConstraint.constant=0;

    if(self.exchangeSegment.selected==0)
    {
        exchangeString=@"BFO";
    }
    
    else if(self.exchangeSegment.selected==1)
    {
        exchangeString=@"NFO";
    }
    
    equityStr=@"FUT";
    


}

-(void)currencyAction
{
    self.equityBtn.selected = NO;
    self.derivativeBtn.selected = NO;
    self.currencyBtn.selected = YES;
    self.commodityBtn.selected = NO;
    self.instrumentLbl.hidden=NO;
    self.instrumentView.hidden=NO;
    self.futureBtn.selected = YES;
    self.instrumentHgt.constant=14;
    self.instrumentTxtHgt.constant=63.2;
    self.instrumentLblTop.constant=24;
    self.instrumentViewTop.constant=10;
    self.optionLbl.hidden=YES;
    self.optionLblHgt.constant=0;
    self.optionViewHgt.constant=0;
    self.optionLblTop.constant=0;
    self.optionViewTop.constant=0;
    self.optionView.hidden=YES;
    self.expiryDateLbl.hidden=NO;
    self.expiryLblHgt.constant=14;
    self.expiryTxtHgt.constant=30;
    self.expiryDateLblTop.constant=24;
    self.expiryDateTFTop.constant=10;
    self.expiryDateTxt.hidden=NO;
    // self.strikePrice.hidden=YES;
    //self.strikeHgt.constant=0;
    //self.strikeTxtHgt.constant=0;
    //self.strikePricelblTop.constant=0;
    //self.strikePriceTFTop.constant=0;
    //self.strikePriceTxt.hidden=YES;
    self.expHeightContraint.constant=63.4;
    self.strikeHeightConstraint.constant=63.4;
    self.lineViewHeightConstraint.constant=1;
    self.spLineViewHeightConstraint.constant=1;
    self.imageViewHeightContraint.constant=30;
    self.spImageViewHeightConstraint.constant=30;
    self.TFHeightConstraint.constant=17;
    self.spTFHeightConstraint.constant=17;
    [self futureAction];
    self.symbolTxt.text=@"";
    self.expiryDateTxt.text=@"";
    self.strikePriceTxt.text=@"";
    
    self.strikeView.hidden=YES;
    self.strikeDropDown.hidden=YES;
    
    self.strikePricelblTop.constant=14;
    exchangeString=@"CDS";


}

-(void)commodityAction
{
    self.equityBtn.selected = NO;
    self.derivativeBtn.selected = NO;
    self.currencyBtn.selected = NO;
    self.commodityBtn.selected = YES;
    self.instrumentLbl.hidden=NO;
    self.instrumentView.hidden=NO;
    self.futureBtn.selected = YES;
     equityStr=@"FUT";
    self.PEBtn.selected=NO;
     exchangeString=@"MCX";
    self.symbolTxt.text=@"";
    self.expiryDateTxt.text=@"";
    self.strikePriceTxt.text=@"";

}

-(void)futureAction
{
//    self.equityBtn.selected = NO;
//    self.derivativeBtn.selected = YES;
//    self.currencyBtn.selected = NO;
//    self.commodityBtn.selected = NO;
    self.instrumentLbl.hidden=NO;
    self.instrumentView.hidden=NO;
    self.futureBtn.selected = YES;
    self.instrumentHgt.constant=14;
    self.instrumentTxtHgt.constant=63.2;
    self.instrumentLblTop.constant=24;
    self.instrumentViewTop.constant=10;
    self.optionLbl.hidden=YES;
    self.optionLblHgt.constant=0;
    self.optionViewHgt.constant=0;
    self.optionLblTop.constant=0;
    self.optionViewTop.constant=0;
    self.optionView.hidden=YES;
    self.expiryDateLbl.hidden=NO;
    self.expiryLblHgt.constant=14;
    self.expiryTxtHgt.constant=30;
    self.expiryDateLblTop.constant=24;
    self.expiryDateTFTop.constant=10;
    self.expiryDateTxt.hidden=NO;
    self.strikePrice.hidden=NO;
    self.strikeHgt.constant=0;
    self.strikeTxtHgt.constant=0;
    self.strikePricelblTop.constant=0;
    self.strikePriceTFTop.constant=14;
    self.strikePriceTxt.hidden=YES;
    self.optionBtn.selected = NO;
    self.strikeHeightConstraint.constant=0;
    self.spImageViewHeightConstraint.constant=0;
    self.spLineViewHeightConstraint.constant=0;
    
    self.strikeView.hidden=YES;
    self.strikeDropDown.hidden=YES;
    
    equityStr=@"FUT";
    


}

-(void)optionAction
{
    self.futureBtn.selected = NO;
    self.optionBtn.selected = YES;
    self.instrumentLbl.hidden=NO;
    self.instrumentHgt.constant=14;
    self.instrumentTxtHgt.constant=63.2;
    self.instrumentLblTop.constant=24;
    self.instrumentViewTop.constant=10;
    self.instrumentView.hidden=NO;
    self.optionLbl.hidden=NO;
    self.optionLblHgt.constant=14;
    self.optionViewHgt.constant=63.2;
    self.optionLblTop.constant=24;
    self.optionViewTop.constant=10;
    self.optionView.hidden=NO;
    self.expiryDateLbl.hidden=NO;
    self.expiryLblHgt.constant=14;
    self.expiryTxtHgt.constant=30;
    self.expiryDateLblTop.constant=24;
    self.expiryDateTFTop.constant=10;
    self.expiryDateTxt.hidden=NO;
    self.strikePrice.hidden=NO;
    self.strikeHgt.constant=14;
    self.strikeTxtHgt.constant=30;
    self.strikePricelblTop.constant=14;
    self.strikePriceTFTop.constant=10;
    self.strikePriceTxt.hidden=NO;
    [self ceAction];
    
    self.strikeView.hidden=NO;
    self.strikeDropDown.hidden=NO;
    
    self.strikeHeightConstraint.constant=63.4;
    self.spImageViewHeightConstraint.constant=30;
    self.spLineViewHeightConstraint.constant=1;
}

-(void)ceAction
{
    self.CE.selected = YES;
    self.PEBtn.selected = NO;
    instrumentTypeStr=@"CE";
     equityStr=@"CE";
}

-(void)peAction
{
    self.CE.selected = NO;
    self.PEBtn.selected = YES;
    instrumentTypeStr=@"PE";
     equityStr=@"PE";
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addSymbol:(id)sender {
    
    
//    NSDictionary *headers = @{ @"x-access-token": delegate1.accessToken,
//                               @"cache-control": @"no-cache",
//                             };
//    
//    NSString * urlStr=[NSString stringWithFormat:@"http://zambaladev.ap-southeast-1.elasticbeanstalk.com/api/stock?symbol=%@&instrumenttype=%@&segment=%@",self.symbolTxt.text,equityStr,exchangeString];
//    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:10.0];
//    [request setHTTPMethod:@"GET"];
//    [request setAllHTTPHeaderFields:headers];
//    
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                    if (error) {
//                                                        NSLog(@"%@", error);
//                                                    } else {
//                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                        NSLog(@"%@", httpResponse);
//                                                        
////                                                        [delegate1.instrumentNameArr removeAllObjects];
//                                                        
//                                                        NSArray * array=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                                                        NSLog(@"%@",array);
//                                                        
//                                                      
//                                                            [delegate1.instrumentNameArr addObject:[[array objectAtIndex:0]objectForKey:@"name"]];
//                                                            
//                                                            NSString * string=[[array objectAtIndex:0]objectForKey:@"instrument_token"];
//                                                            
//                                                            
//                                                            int myInt = [string intValue];
//                                                            
//                                                        NSLog(@"token%i",myInt);
//                                                        
//                                                            [delegate1.instrumentToken addObject:[NSNumber numberWithInt:myInt]];
//                                                            
//                                                        NSLog(@"array %@",delegate1.instrumentToken);
//                                                        
//                                                           
//                                                         }
//                                                   
//                                                    
//                                                      dispatch_async(dispatch_get_main_queue(), ^{
//                                                          
////                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
////                                                          
////                                                          [self.navigationController pushViewController:home1 animated:YES];
//                                                          
//                                                           [self.navigationController popViewControllerAnimated:YES];
//                                                      });
//                                                }];
//    [dataTask resume];
    
    NSLog(@"%@",self.detailsDict);
    
    //NSString * detailsString=[[self.detailsDict objectForKey:@"details" ]objectAtIndex:objectAtIndex];
    
   // NSArray * detailsArray=@[detailsString];
    
    NSArray * detailsArray = [[self.detailsDict objectForKey:@"details" ]objectAtIndex:objectAtIndex];
    NSLog(@"array %@",detailsArray);
    
    NSString * string=[detailsArray valueForKey:@"instrument_token"];
    
    
                                                                int myInt = [string intValue];
    
                                                            NSLog(@"token%i",myInt);
    
    
    [delegate1.instrumentToken addObject:[NSNumber numberWithInt:myInt]];
    
    delegate1.exchange= [detailsArray valueForKey:@"exchange"];
    
    [delegate1.instrumentNameArr addObject:[detailsArray valueForKey:@"symbol"]];
    NSLog(@"array %@",delegate1.instrumentNameArr);

    [delegate1.filteredCompanyName addObject:detailsArray];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    
//    if(textField==self.symbolTxt)
//    {
//        
//        if([equityStr isEqualToString:@"EQ"]&&[exchangeString isEqualToString:@"BSE"])
//        {
//           }
//}


- (UITableViewCell *)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
        static NSString *cellIdentifier = @"WeekAutoSuggestionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
    NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
    
    cell.textLabel.text = weeks[indexPath.row];
    
    return cell;
}

- (NSInteger)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section forText:(NSString *)text {
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
    
    @try {
        NSInteger count = [self.symbols filteredArrayUsingPredicate:filterPredictate].count;
        return count;
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    }

    return 0;
    
}

- (void)autoSuggestionField:(UITextField *)field textChanged:(NSString *)text {
    [self loadWeekDays];
}

- (CGFloat)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    return 50;
}

- (void)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    NSLog(@"Selected suggestion at index row - %ld", (long)indexPath.row);
    
           NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
        NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
        
        self.symbolTxt.text = weeks[indexPath.row];
    
    NSIndexPath *selectedIndexPath = [tableView indexPathForSelectedRow];
    
   objectAtIndex = selectedIndexPath.row;
    
    NSLog(@"%li",(long)objectAtIndex);
    
    self.expiryDateTxt.text=[expiry objectAtIndex:objectAtIndex];
    self.strikePriceTxt.text=[strike objectAtIndex:objectAtIndex];
    
    
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return true;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.searchStr = [self.symbolTxt.text stringByReplacingCharactersInRange:range withString:string];
    
    NSLog(@"%@",self.searchStr);
    
    if(self.searchStr.length>=3)
    {
       
        NSLog(@"%@",delegate1.accessToken);
        NSDictionary *headers = @{ @"x-access-token": delegate1.accessToken,
                                   @"cache-control": @"no-cache",
                                   };
        
        NSString * urlStr=[NSString stringWithFormat:@"http://zambaladev.ap-southeast-1.elasticbeanstalk.com/api/stock?symbol=%@&instrumenttype=%@&segment=%@",self.searchStr,equityStr,exchangeString];
        
        NSLog(@"url %@",urlStr);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            [self.company removeAllObjects];
                                                            
                                                            [expiry removeAllObjects];
                                                            [strike removeAllObjects];
                                                            ;
                                                            
                                                            self.responseArray=[[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] mutableCopy];
                                                            NSLog(@"%@",self.responseArray);
                                                            
                                                            NSString * string=[NSString stringWithFormat:@"%@",self.responseArray];
                                                            
                                                            self.detailsDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSArray arrayWithArray:self.responseArray],@"details", nil];
                                                            
                                                            for(int i=0;i<self.responseArray.count;i++)
                                                            {
                                                                
                                                                [self.company addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                                
                                                                [expiry addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"expiry"]];
                                                                
                                                                 [strike addObject:[[self.responseArray objectAtIndex:i]objectForKey:@"strikeprice"]];
                                                                
                                                            }
                                                            
                                                             [self.responseArray removeAllObjects];
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            //                                                          HomePage * home1=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                                                            //
                                                            //                                                          [self.navigationController pushViewController:home1 animated:YES];
                                                            
                                                            
                                                            
                                                        });
                                                    }];
        [dataTask resume];
        
    }
    
    return YES;
}



@end
