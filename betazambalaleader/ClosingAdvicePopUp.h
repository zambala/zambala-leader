//
//  ClosingAdvicePopUp.h
//  betazambalaleader
//
//  Created by zenwise mac 2 on 3/24/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClosingAdvicePopUp : UIViewController
@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIButton *closeAdviceButton;
- (IBAction)cancelButtonTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;

@end
