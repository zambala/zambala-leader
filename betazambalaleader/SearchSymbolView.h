//
//  SearchSymbolView.h
//  testing
//
//  Created by zenwise technologies on 16/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchSymbolView : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *equityBtn;


@property (strong, nonatomic) IBOutlet UIButton *derivativeBtn;


@property (strong, nonatomic) IBOutlet UIButton *currencyBtn;

@property (strong, nonatomic) IBOutlet UIButton *commodityBtn;


@property (strong, nonatomic) IBOutlet UITextField *symbolTxt;


@property (strong, nonatomic) IBOutlet UIButton *futureBtn;


@property (strong, nonatomic) IBOutlet UIButton *optionBtn;



@property (strong, nonatomic) IBOutlet UIButton *CE;

@property (strong, nonatomic) IBOutlet UIButton *PEBtn;

@property (strong, nonatomic) IBOutlet UISegmentedControl *exchangeSegment;

@property (strong, nonatomic) IBOutlet UIScrollView *scroll;


- (IBAction)addSymbol:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *instrumentView;


@property (strong, nonatomic) IBOutlet UILabel *instrumentLbl;


@property (strong, nonatomic) IBOutlet UILabel *expiryDateLbl;


@property (strong, nonatomic) IBOutlet UITextField *expiryDateTxt;

@property (strong, nonatomic) IBOutlet UILabel *strikePrice;


@property (strong, nonatomic) IBOutlet UITextField *strikePriceTxt;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *TFHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *spTFHeightConstraint;


@property (strong, nonatomic) IBOutlet UILabel *optionLbl;
@property (strong, nonatomic) IBOutlet UIView *symbolLabelHeightConstraint;

@property (strong, nonatomic) IBOutlet UIView *optionView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *expHeightContraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *strikeHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lineViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightContraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *spLineViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *spImageViewHeightConstraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *optionLblHgt;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *optionViewHgt;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *expiryLblHgt;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *expiryTxtHgt;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *strikeHgt;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *strikeTxtHgt;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *instrumentHgt;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *instrumentTxtHgt;

@property NSMutableArray * company;


@property (strong, nonatomic) IBOutlet UITableView *searchTableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *instrumentLblTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *instrumentViewTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *optionLblTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *optionViewTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *expiryDateLblTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *expiryDateTFTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *strikePricelblTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *strikePriceTFTop;

@property NSMutableArray * symbols;
@property NSString * searchStr;
@property NSMutableArray * responseArray;
@property NSDictionary * detailsDict;

@property (strong, nonatomic) IBOutlet UIView *strikeView;
@property (strong, nonatomic) IBOutlet UIImageView *strikeDropDown;

@end
