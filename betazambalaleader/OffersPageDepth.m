//
//  ViewController2.m
//  OfferPage
//
//  Created by guna on 03/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "OffersPageDepth.h"
#import "CollectionViewCell1.h"
#import "TableViewCell1.h"
#import "OffersPageDetail.h"

@interface OffersPageDepth ()
{
    NSMutableArray * imageArray;
}


@end

@implementation OffersPageDepth

- (void)viewDidLoad {
    [super viewDidLoad];
//    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    imageArray=[[NSMutableArray alloc]initWithObjects:@"travelStay.png",@"entertainment.png",@"shopping.png",@"food.png",@"homeLifestyle.png",@"sportsFitness.png",@"food.png", nil];
    
    self.offerCollectionView.delegate=self;
    self.offerCollectionView.dataSource=self;
    self.offerTableView.delegate=self;
    self.offerTableView.dataSource=self;
    self.offerView.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.18f]CGColor];
    self.offerView.layer.shadowOffset = CGSizeMake(0, 2);
    self.offerView.layer.shadowOpacity = 5;
    self.offerView.layer.shadowRadius = 4.2;
    self.offerView.clipsToBounds = NO;
    self.shopButton.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.18f]CGColor];
    self.shopButton.layer.shadowOffset = CGSizeMake(1, 1.7);
    self.shopButton.layer.shadowOpacity = 1;
    self.shopButton.layer.shadowRadius = 2.1;
    self.shopButton.clipsToBounds = NO;
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return 3;
    
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    TableViewCell1 * cell=[tableView dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
    
    cell.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.18f]CGColor];
    cell.layer.shadowOffset = CGSizeMake(0, 2);
    cell.layer.shadowOpacity = 1;
    cell.layer.shadowRadius = 4.2;
    cell.clipsToBounds = NO;

    
    //    cell.overHeadCostLabel.text=[self.overHeadCostArray objectAtIndex:indexPath.row];
    //    cell.investimentLabel.text=[self.investimentArray objectAtIndex:indexPath.row];
    //    cell.revenueLabel.text=[self.revenueArray objectAtIndex:indexPath.row];
    //    cell.yearLabel.text=[self.yearArray objectAtIndex:indexPath.row];
    
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 70;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    
    
    
    return imageArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //    static NSString *identifier = @"Cell";
    
    
    CollectionViewCell1 *cell = [self.offerCollectionView dequeueReusableCellWithReuseIdentifier:@"Cell2" forIndexPath:indexPath];
    cell.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.18f]CGColor];
    cell.layer.shadowOffset = CGSizeMake(0, 2);
    cell.layer.shadowOpacity = 1;
    cell.layer.shadowRadius = 4.2;
    cell.clipsToBounds = NO;

    
    cell.imageView.image=[UIImage imageNamed:[imageArray objectAtIndex:indexPath.row]];
    
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@" index %ld",(long)indexPath.row);
}
    //        ViewController1 * VC=[self.storyboard instantiateViewControllerWithIdentifier:@"data"];
    //        [self.navi
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
//            ViewController1 * VC=[self.storyboard instantiateViewControllerWithIdentifier:@"data"];
//            [self.navigationController pushViewController:VC animated:YES];
    
}
- (IBAction)backButtonTap:(id)sender {
    
    OffersPageDetail *offerPage = [self.storyboard instantiateViewControllerWithIdentifier:@"data"];
    [self presentViewController:offerPage animated:YES completion:nil];
}
@end
