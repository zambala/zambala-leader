//
//  HomePage.h
//  testing
//
//  Created by zenwise mac 2 on 11/15/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonHMAC.h>
#import "PSWebSocket.h"




@interface HomePage : UIViewController<UITableViewDelegate, UITableViewDataSource>



@property NSMutableURLRequest * urlRequest1;
@property NSURLSession * session1;
@property NSURLSessionDataTask * task2;

@property NSMutableURLRequest * urlRequest2;
@property NSURLSession * session2;
@property NSURLSessionDataTask * task3;

@property (nonatomic, strong) PSWebSocket *socket;
@property NSString * jsonString;

@property (assign) BOOL echo;

@property (assign) BOOL isFinished;
@property (assign) BOOL isExecuting;

@property NSMutableArray * arr;
@property (strong, nonatomic) IBOutlet UIView *ifEmptyView;


@property (weak, nonatomic) IBOutlet UITableView *marketTableView;

- (IBAction)editTableViewBtn:(id)sender;

@property NSMutableArray * companyArray;

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property NSMutableArray * companyArray1;

@property id message;
//@property SRWebSocket * webSocket;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
