//
//  CloseAdvicePopUp.h
//  betazambalaleader
//
//  Created by zenwise mac 2 on 3/24/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CloseAdvicePopUp : UIViewController


- (IBAction)cancelBtn:(id)sender;

- (IBAction)yesBtn:(id)sender;

@end
