//
//  NewPasswordEnter.m
//  passwordLeader
//
//  Created by zenwise mac 2 on 4/17/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewPasswordEnter.h"
#import "PasswordSuccess.h"

@interface NewPasswordEnter ()

@end

@implementation NewPasswordEnter

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onResetPasswordTap:(id)sender {
    
    if([self.NPTF.text isEqualToString:self.CPTF.text])
    {
        PasswordSuccess * passwordSuccess=[self.storyboard instantiateViewControllerWithIdentifier:@"PasswordSuccess"];
        [self.navigationController pushViewController:passwordSuccess animated:YES];
    }
    else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Passwords does not match" message:@"Please check the password." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
    }
    
}
@end
