//
//  ViewController2.h
//  OfferPage
//
//  Created by guna on 03/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersPageDepth : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *offerTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *offerCollectionView;
- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *offerView;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backButtonTap:(id)sender;

@end
