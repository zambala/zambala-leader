//
//  NewPasswordEnter.h
//  passwordLeader
//
//  Created by zenwise mac 2 on 4/17/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewPasswordEnter : UIViewController
- (IBAction)onResetPasswordTap:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *NPTF;
@property (strong, nonatomic) IBOutlet UITextField *CPTF;

@end
