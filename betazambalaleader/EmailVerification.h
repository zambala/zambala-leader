//
//  EmailVerification.h
//  passwordLeader
//
//  Created by zenwise mac 2 on 4/17/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailVerification : UIViewController
- (IBAction)onNextButtonTap:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *emailTF;

@end
