//
//  PopUpView.m
//  zambala leader
//
//  Created by zenwise mac 2 on 1/4/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "PopUpView.h"
#import "UIViewController+MJPopupViewController.h"
#import "AppDelegate.h"
#import "MyAdviceView.h"
#import "PSWebSocket.h"
#import "NewAdvice.h"

@interface PopUpView () <PSWebSocketDelegate>

{
    AppDelegate *delegate;
    bool temp;
    int value;
    int packetsLength;
    int packetsNumber;
    float lastPriceFlaotValue;
    float closePriceFlaotValue;
    NSMutableDictionary * instrumentTokensDict;
    NewAdvice * newadvice;
    NSString * string1;
    
    float changeFloatValue;
    
    float changeFloatPercentageValue;
}

@end

@implementation PopUpView

- (void)viewDidLoad {
    
    delegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_chkBoxBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [_chkBoxBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    _chkBoxBtn.selected = NO;
    NSLog(@"Delegate String pop up%@",delegate.segmentString);
    instrumentTokensDict=[[NSMutableDictionary alloc]init];
    self.symbolNameDict= [[NSMutableDictionary alloc]init];
    self.companyNameDict =[[NSMutableDictionary alloc]init];
    //activity indicator//
    
    self.activityIndicator.hidden=NO;
    
    _messageLbl.text = _messageStr1;
    _symbolLbl.text = _symbolStr;
    
    NSLog(@"Symbol string VDL%@:",self.symbolStr);
    [self.companyNameDict  setObject:self.symbolStr forKey:@"companyname"];
    
    NSLog(@"Company name VDL:%@",self.companyNameDict);
    
    _targetLbl.text = [NSString stringWithFormat:@"%@.00", _targetPriceStr];
    _entryLbl.text = [NSString stringWithFormat:@"%@.00", _entryPriceStr];
    _stopLossLbl.text = [NSString stringWithFormat:@"%@.00", _stopLossStr];
    
   [ _cancelBtn setTitleColor:RGB(11, 122, 179) forState:UIControlStateNormal];
    [ _postBtn setTitleColor:RGB(11, 122, 179) forState:UIControlStateNormal];
   
    _lbl1.layer.cornerRadius = 12.0f;
    _lbl2.layer.cornerRadius = 12.0f;
    _lbl1.layer.masksToBounds = YES;
    _lbl2.layer.masksToBounds = YES;

    _lbl1.backgroundColor = RGB(231, 48, 50);
    _lbl2.backgroundColor = RGB(154, 217, 240);

    
    [_premiumImg setHidden:YES];
    [_premiumLbl setHidden:YES];

    
    if ([_premiumStr isEqualToString:@"ON"])
    {
        [_premiumImg setHidden:NO];
        [_premiumLbl setHidden:NO];
    }
    else
    {
        [_premiumImg setHidden:YES];
        [_premiumLbl setHidden:YES];
    }
    
    if ([_chkBoxStr isEqualToString:@"TICK"])
    {
        _chkBoxBtn.selected = YES;
    }
    else
    {
        _chkBoxBtn.selected = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    
    //pocket socket//
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://websocket.kite.trade/?api_key=%@&user_id=%@&public_token=%@",delegate.APIKey,delegate.userID,delegate.publicToken]];
    
    NSLog(@"user %@",delegate.userID);
    NSLog(@"user %@",delegate.publicToken);
    
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    //     create the socket and assign delegate
    self.socket = [PSWebSocket clientSocketWithRequest:request];
    self.socket.delegate = self;
    //
    //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    //     open socket
    
    //        if(delegate2.instrumentToken.count>0)
    //    {
    //
    //    }
    
    if(delegate.leaderInstrumentToken.count>0)
    {
       
        [self.socket open];
        
    }else
    {
        self.activityIndicator.hidden=YES;
        
    }
    
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)chkBoxAction:(id)sender
{
    if (checkBoxFlag == false)
    {
        _chkBoxBtn.selected = YES;
        checkBoxFlag = true;
    }
    else
    {
        _chkBoxBtn.selected = NO;
        checkBoxFlag = false;
    }
}

-(IBAction)postAction:(id)sender
{
    
    NSNumber * segmentNumber = [NSNumber numberWithInt:delegate.segmentInt];
    NSMutableArray * segmentArray = [[NSMutableArray alloc]initWithObjects:segmentNumber, nil];
    NSString * segmentString = [NSString stringWithFormat:@"%@",[segmentArray objectAtIndex:0]];
    NSLog(@"Segment Array:%@",segmentArray);
    NSLog(@"Segment String:%@",segmentString);
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    NSDictionary *headers = @{@"x-access-token":delegate.accessToken,
                              @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                              
                               };
    
    NSLog(@"post action company name:%@",self.companyNameDict);
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    

    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSString * segmentValue = delegate.segmentString;
    NSLog(@"Segment Value%@",segmentValue);
    NSString *postString =[NSString stringWithFormat:@"userid=4&messagetypeid=6&subscriptiontypeid=%@&message=EICHADV;Entry=%@;EP=%@;SL=%@&messagename=EICHADV&amount=250.5&deleted=1&buysell=%@&duration=%@&instrumentid=738561&entryprice=%@&targetprice=%@&stoploss=%@&companyname=%@&segment=%@",self.premiumAdvice, _entryLbl.text, _targetLbl.text, _stopLossLbl.text,_messageStr,self.duration,_entryLbl.text, _targetLbl.text, _stopLossLbl.text,[self.companyNameDict allValues],segmentArray];
    
    NSString * ep=self.entryLbl.text;
    
    float epFloat=[ep floatValue];
    
    NSNumber * epNumber=[NSNumber numberWithFloat:epFloat];
    
    NSString * sp=self.stopLossLbl.text;
    
    float spFloat=[sp floatValue];
    
    NSNumber * spNumber=[NSNumber numberWithFloat:spFloat];
    
    
    NSString * tp=self.targetLbl.text;
    
    float tpFloat=[tp floatValue];
    
    NSNumber * tpNumber=[NSNumber numberWithFloat:tpFloat];
    
    
    
    
//    NSDictionary *parameters = @{ @"userid": @4,
//                                  @"messagetypeid": @6,
//                                  @"subscriptiontypeid": _premiumAdvice,
//                                  @"message":@"EICHADV"
//                                  @"Entry":_entryLbl.text;
//                                  @"messagename": self.portFolioNameStr,
//                                  @"amount": self.assumedInvestiment.text,
//                                  @"portfolioinfo": userDictionary,
//                                  @"segment":self.segmentString};

   
    NSDictionary * parameters1=@{@"userid":@4,
                                 @"messagetypeid":@6,
                                 @"subscriptiontypeid":self.premiumAdvice,
                                     
                                 @"amount":@250.5,
                                 @"deleted":@1,
                                 @"buysell":self.messageStr,
                                     @"duration":self.duration,
                                 @"instrumentid":@738561,
                                 @"entryprice":epNumber,
                                     @"targetprice":tpNumber,
                                     @"stoploss":spNumber,
                                 @"companyname":[self.companyNameDict allValues],
                
                                 @"segment":segmentArray
                                 };
    
       NSLog(@"post string:%@",postString);
    NSLog(@"params %@",parameters1);

//    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters1 options:0 error:nil];


    NSMutableData *postData = [[NSMutableData alloc] initWithData:[postString dataUsingEncoding:NSUTF8StringEncoding]];

     NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://zambaladev.ap-southeast-1.elasticbeanstalk.com/api/messages"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            
            NSDictionary *orderResponse=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"orderResponse----%@",orderResponse);
            
            self.responseStr=[orderResponse objectForKey:@"message"];
            
            

        }

        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if([self.responseStr isEqualToString:@"Message created"])
            {
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Advice Posted" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
                    
                }];
                
                [alert addAction:okAction];
                
            }
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                
                
                [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
                
            });
            
        });
        
        
    }];
    
    [postDataTask resume];

}


-(IBAction)cancelAction:(id)sender
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];

}

//websockets//

- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
    NSLog(@"The websocket handshake completed and is now open!");
    
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
    
    
    //     instrumentTokens=[[NSMutableArray alloc]init];
    
    
    
    //   [instrumentTokens addObject:[NSNumber numberWithInt:11547906]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:4911105]];
    //   [instrumentTokens addObject:[NSNumber numberWithInt:11508226]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:11535618]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:24026370]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:12344066]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:4954113]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:20517634]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:20517890]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:217138949]];
    
    if(delegate.leaderInstrumentToken.count>0)
    {
        
        NSDictionary * subscribeDict=@{@"a": @"subscribe", @"v":delegate.leaderInstrumentToken};
        
        
        NSData *json;
        
        NSError * error;
        
        
        
        
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:subscribeDict])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
            
            // If no errors, let's view the JSON
            if (json != nil && error == nil)
            {
                string1 = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                
                //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                //
                
                NSLog(@"JSON: %@",string1);
                
            }
        }
        
        
        [self.socket send:string1];
        
        
        
        
        
        
        NSArray * mode=@[@"full",delegate.leaderInstrumentToken];
        
        subscribeDict=@{@"a": @"mode", @"v": mode};
        
        
        
        //     Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:subscribeDict])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
            
            
            // If no errors, let's view the JSON
            if (json != nil && error == nil)
            {
                _message = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                
                //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                
                NSLog(@"JSON: %@",_message);
                
            }
        }
        [self.socket send:_message];
        
    }
    
    
}




- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    
    
    
    
    
    NSLog(@"The websocket received a message: %@",message);
    
    
    
    NSData * data=[NSKeyedArchiver archivedDataWithRootObject:message];
    
    
    
    
    
    if(data.length > 247)
    {
        
        
        NSLog(@"The websocket received a message: %@",message);
        
        
        id packets = [message subdataWithRange:NSMakeRange(0,2)];
        packetsNumber = CFSwapInt16BigToHost(*(int16_t*)([packets bytes]));
        NSLog(@" number of packets %i",packetsNumber);
        
        int startingPosition = 2;
        
        for( int i =0; i< packetsNumber ; i++)
        {
            
            
            NSData * packetsLengthData = [message subdataWithRange:NSMakeRange(startingPosition,2)];
            packetsLength = CFSwapInt16BigToHost(*(int16_t*)([packetsLengthData bytes]));
            startingPosition = startingPosition + 2;
            
            id packetQuote = [message subdataWithRange:NSMakeRange(startingPosition,packetsLength)];
            
            
            
            
            id instrumentTokenData = [packetQuote subdataWithRange:NSMakeRange(0, 4)];
            int32_t instrumentTokenValue = CFSwapInt32BigToHost(*(int32_t*)([instrumentTokenData bytes]));
            
            id lastPrice = [packetQuote subdataWithRange:NSMakeRange(4,4)];
            int32_t lastPriceValue = CFSwapInt32BigToHost(*(int32_t*)([lastPrice bytes]));
            
            
            
            
            
            
            id closingPrice = [packetQuote subdataWithRange:NSMakeRange(40,4)];
            int32_t closePriceValue = CFSwapInt32BigToHost(*(int32_t*)([closingPrice bytes]));
            
            
            
            
            
            
            //                id marketDepth = [packetQuote subdataWithRange:NSMakeRange(44,120)];
            //                int32_t marketDepthValue = CFSwapInt32BigToHost(*(int32_t*)([marketDepth bytes]));
            //
            //                NSLog(@"%@",marketDepth);
            
            if([delegate.exchange isEqualToString:@"CDS"])
            {
                lastPriceFlaotValue = (float)lastPriceValue/10000000;
                
                closePriceFlaotValue = (float)closePriceValue/10000000;
                
                NSLog(@"CLOSE %f",closePriceFlaotValue);
                
                changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                
                changeFloatPercentageValue = (changeFloatValue/closePriceFlaotValue)/100;
            }
            else
            {
                lastPriceFlaotValue = (float)lastPriceValue/100;
                
                closePriceFlaotValue = (float)closePriceValue/100;
                
                NSLog(@"CLOSE %f",closePriceFlaotValue);
                
                changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                
                changeFloatPercentageValue = (changeFloatValue/closePriceFlaotValue)/100;
            }
            
            startingPosition = startingPosition + packetsLength;
            
            
            @autoreleasepool {
                
                NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc]init];
                [tmpDict setValue:[NSString stringWithFormat:@"%d",instrumentTokenValue] forKey:@"InstrumentToken"];
                
                NSString *tmpInstrument = [NSString stringWithFormat:@"%d",instrumentTokenValue];
                
                [tmpDict setValue:[NSString stringWithFormat:@"%.2f",closePriceFlaotValue] forKey:@"ClosePriceValue"];
                
                [tmpDict setValue:[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue] forKey:@"LastPriceValue"];
                
                [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatValue] forKey:@"ChangeValue"];
                
                [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatPercentageValue] forKey:@"ChangePercentageValue"];
                NSLog(@"%@",tmpDict);
                
                
                [instrumentTokensDict setValue:tmpDict forKeyPath:@"value"];
                
                NSLog(@"%@",instrumentTokensDict);
                
                
               
                
                
                //  [shareListArray addObject:tmpDict];
                
            }
            
            
        }
        self.ltpLbl.text=[[instrumentTokensDict objectForKey:@"value"]objectForKey:@"LastPriceValue"];
        NSLog(@"%@",self.ltpLbl.text);
        self.chngLbl.text=[[instrumentTokensDict objectForKey:@"value" ]objectForKey:@"ChangePercentageValue"];
        
               //  [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
        
        [self.activityIndicator stopAnimating];
        self.activityIndicator.hidden=YES;
        
               
        
        
    }
    
    
}


- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"The websocket handshake/connection failed with an error: %@", error);
}
- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data
{
    NSLog(@"The websocket received a message: %@", data);
}
- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
}



@end
