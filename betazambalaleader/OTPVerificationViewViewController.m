//
//  OTPVerificationViewViewController.m
//  testing
//
//  Created by zenwise technologies on 23/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "OTPVerificationViewViewController.h"
#import "NewPasswordEnter.h"
#import "AppDelegate.h"

@interface OTPVerificationViewViewController ()

@end

@implementation OTPVerificationViewViewController
{
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];

    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.OTPString=textField.text;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard
{
    [self.OTPText resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)verifyAction:(id)sender {
    
    
    
    if(self.OTPText.text.length>0)
    {
        self.OTPString=self.OTPText.text;
        NSLog(@"OTP String:%@",self.OTPString);
        
       // NSData *postData = [[NSData alloc] initWithData:[@"{}" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                   };

        NSString * url=[NSString stringWithFormat:@"http://2factor.in/API/V1/13ffd06a-2fe3-11e7-8473-00163ef91450/SMS/VERIFY/%@/%@",delegate.phoneNumberSessionID,self.OTPString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            self.responseDictonary= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            NSLog(@"OTP dict%@",self.responseDictonary);
                                                            
                                                            

                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            if([[self.responseDictonary objectForKey:@"Details"]isEqualToString:@"OTP Matched"])
                                                            {
                                                                NewPasswordEnter * otp=[self.storyboard instantiateViewControllerWithIdentifier:@"NewPasswordEnter"];
                                                                [self presentViewController:otp animated:YES completion:nil];
                                                            }
                                                            else
                                                            {
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Invalid OTP. Try Again." preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
                                                                
                                                                [alert addAction:okAction];
                                                            }

                                                            
                                                            
                                                            
                                                            
                                                            
                                                        });

                                                        
                                                       
                                                    }];
        [dataTask resume];
        
        
    }
    else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter OTP" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        

    }
}

- (IBAction)resendAction:(id)sender {
}
@end
