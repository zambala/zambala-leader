//
//  ViewController1.m
//  OfferPage
//
//  Created by guna on 03/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "OffersPageDetail.h"
#import "TableViewCell.h"
#import "OffersPage.h"

@interface OffersPageDetail ()

@end

@implementation OffersPageDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listTableView.delegate=self;
    self.listTableView.dataSource=self;
    // Do any additional setup after loading the view.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return 5;
    
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    TableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.18f]CGColor];
    cell.layer.shadowOffset = CGSizeMake(0, 2);
    cell.layer.shadowOpacity = 1;
    cell.layer.shadowRadius = 4.2;
    cell.clipsToBounds = NO;

    
//    cell.overHeadCostLabel.text=[self.overHeadCostArray objectAtIndex:indexPath.row];
//    cell.investimentLabel.text=[self.investimentArray objectAtIndex:indexPath.row];
//    cell.revenueLabel.text=[self.revenueArray objectAtIndex:indexPath.row];
//    cell.yearLabel.text=[self.yearArray objectAtIndex:indexPath.row];
    
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 120;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onBackButtonTap:(id)sender {
    
    OffersPage *offersPage = [self.storyboard instantiateViewControllerWithIdentifier:@"offers"];
    [self presentViewController:offersPage animated:YES completion:nil];
    
}
@end
