//
//  ViewController.m
//  OfferPage
//
//  Created by guna on 03/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "OffersPage.h"
#import "PageImageScrollView.h"
#import "CollectionViewCell.h"
#import "OffersPageDetail.h"
#import "MainView.h"
#import "TabBar.h"

@interface OffersPage ()
{
    NSMutableArray * imageArray;
}

@end

@implementation OffersPage

- (void)viewDidLoad {
    [super viewDidLoad];
    PageImageScrollView *pageScrollView = [[PageImageScrollView alloc] initWithFrame:CGRectMake(0,0,self.pageView.frame.size.width,self.pageView.frame.size.height)];
    [pageScrollView setScrollViewContents:@[[UIImage imageNamed:@"travelStay.png"], [UIImage imageNamed:@"entertainment.png"], [UIImage imageNamed:@"shopping.png"], [UIImage imageNamed:@"homeLifestyle.png"]]];
    //easily setting pagecontrol pos, see PageControlPosition defination in PagedImageScrollView.h
    pageScrollView.pageControlPos = PageControlPositionCenterBottom;
    
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    
    [self.pageView addSubview:pageScrollView];
    
    imageArray=[[NSMutableArray alloc]initWithObjects:@"travelStay.png",@"entertainment.png",@"shopping.png",@"food.png",@"homeLifestyle.png",@"sportsFitness.png",@"food.png", nil];
    // Do any additional setup after loading the view, typically from a nib.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    self.collectionViewHeight.constant=imageArray.count*150;
    
    self.offerViewHgtConstant.constant=imageArray.count*150+100;
    
    self.scrollView.contentSize=CGSizeMake(300,imageArray.count*150+200);
    
    return imageArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //    static NSString *identifier = @"Cell";
    
        
        CollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"cell1" forIndexPath:indexPath];
    cell.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.18f]CGColor];
    cell.layer.shadowOffset = CGSizeMake(0, 2);
    cell.layer.shadowOpacity = 1;
    cell.layer.shadowRadius = 4.2;
    cell.clipsToBounds = NO;

    
        cell.imageView.image=[UIImage imageNamed:[imageArray objectAtIndex:indexPath.row]];
        
        
        return cell;
    }
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        NSLog(@" index %ld",(long)indexPath.row);
//        ViewController1 * VC=[self.storyboard instantiateViewControllerWithIdentifier:@"data"];
//        [self.navigationController pushViewController:VC animated:YES];
        
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)onBackButtonTap:(id)sender {
    
    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
    
    [self presentViewController:tabPage animated:YES completion:nil];
}
@end
