//
//  CloseAdvicePopUp.m
//  betazambalaleader
//
//  Created by zenwise mac 2 on 3/24/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "CloseAdvicePopUp.h"
#import "UIViewController+MJPopupViewController.h"
#import "AppDelegate.h"

@interface CloseAdvicePopUp ()
{
    AppDelegate * delegate2;
}

@end

@implementation CloseAdvicePopUp

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelBtn:(id)sender {
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

}

- (IBAction)yesBtn:(id)sender {
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

}
@end
