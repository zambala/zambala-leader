//
//  EmailVerification.m
//  passwordLeader
//
//  Created by zenwise mac 2 on 4/17/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "EmailVerification.h"
#import "GuestViewController.h"

@interface EmailVerification ()

@end

@implementation EmailVerification

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)dismissKeyboard
{
    [self.emailTF resignFirstResponder];
}

- (IBAction)onNextButtonTap:(id)sender {
    
   // if([self.emailTF.text isEqualToString:@""])
    //{
        GuestViewController * otp=[self.storyboard instantiateViewControllerWithIdentifier:@"GuestViewController"];
        [self presentViewController:otp animated:YES completion:nil];
    //}
    //else
    //{
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"ID not found" message:@"The email ID you entered is not registered with us. Please try again with correct details." preferredStyle:UIAlertControllerStyleAlert];
//        
//        [self presentViewController:alert animated:YES completion:^{
//            
//        }];
//        
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            
//        }];
//        
//        [alert addAction:okAction];
    //}
}
@end
