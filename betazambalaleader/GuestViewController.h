//
//  GuestViewController.h
//  testing
//
//  Created by zenwise technologies on 22/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuestViewController : UIViewController<UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UITextField *numberTxtField;

- (IBAction)sendAction:(id)sender;
@property NSMutableDictionary * responseDict;


@end
