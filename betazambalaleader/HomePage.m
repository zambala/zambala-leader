//
//  HomePage.m
//  testing
//
//  Created by zenwise mac 2 on 11/15/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "HomePage.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonHMAC.h>
#import "PSWebSocket.h"

#import "MarketTableViewCell.h"

#import "PSWebSocketDriver.h"

#import<malloc/malloc.h>






@interface HomePage () <PSWebSocketDelegate>
{
    AppDelegate * delegate2;
    NSDictionary * dict1;
    NSString * string1;
    NSMutableArray * LtpArray;
    NSMutableArray * changePerArray;
    NSMutableArray * shareListArray;
    NSMutableArray * shareListArrayCopy;
    NSMutableArray * instrumentTokens;
    NSMutableArray * filteredArray;
    NSMutableDictionary *instrumentTokensDict;
    NSMutableArray *allKeysList;
    
    bool temp;
    int value;
    int packetsLength;
    int packetsNumber;
    float lastPriceFlaotValue;
    float closePriceFlaotValue;
    
    
    float changeFloatValue;
    
    float changeFloatPercentageValue;
    

    
}

@end

@implementation HomePage

- (void)viewDidLoad {
    [super viewDidLoad];
    
    temp = YES;
    
    value  =0 ;
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
       self.arr=[[NSMutableArray alloc]init];
    
    dict1=[[NSDictionary alloc]init];
    shareListArray = [[NSMutableArray alloc]init];
    instrumentTokensDict = [[NSMutableDictionary alloc]init];
    allKeysList = [[NSMutableArray alloc]init];
    
    self.companyArray1=[[NSMutableArray alloc]initWithObjects:@"AHLUCONT-BE",@"TATAMOTORS", nil];
    
   self.navigationItem.title = @"MARKET WATCH";
    
    
    _headerView.frame = CGRectMake(_headerView.frame.origin.x,64,_headerView.frame.size.width , _headerView.frame.size.height);

    
    LtpArray=[[NSMutableArray alloc]init];
    changePerArray=[[NSMutableArray alloc]init];
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
    
    
    
   }

- (void)viewDidAppear:(BOOL)animated
{
    
    //pocket socket//
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://websocket.kite.trade/?api_key=%@&user_id=%@&public_token=%@",delegate2.APIKey,delegate2.userID,delegate2.publicToken]];
    
    NSLog(@"user %@",delegate2.userID);
    NSLog(@"user %@",delegate2.publicToken);
    
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    //     create the socket and assign delegate
    self.socket = [PSWebSocket clientSocketWithRequest:request];
    self.socket.delegate = self;
    //
    //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    //     open socket
    
//        if(delegate2.instrumentToken.count>0)
//    {
//       
//    }
    
    if(delegate2.instrumentToken.count>0)
    {
        self.ifEmptyView.hidden=YES;
        self.marketTableView.hidden=NO;
        [self.socket open];

    }else
    {
        self.activityIndicator.hidden=YES;
        self.ifEmptyView.hidden=NO;
        self.marketTableView.hidden=YES;
    }
    

    


}









- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
    NSLog(@"The websocket handshake completed and is now open!");
    
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
   
    
//     instrumentTokens=[[NSMutableArray alloc]init];
    

   
//   [instrumentTokens addObject:[NSNumber numberWithInt:11547906]];
//    [instrumentTokens addObject:[NSNumber numberWithInt:4911105]];
//   [instrumentTokens addObject:[NSNumber numberWithInt:11508226]];
//    [instrumentTokens addObject:[NSNumber numberWithInt:11535618]];
//    [instrumentTokens addObject:[NSNumber numberWithInt:24026370]];
//    [instrumentTokens addObject:[NSNumber numberWithInt:12344066]];
//    [instrumentTokens addObject:[NSNumber numberWithInt:4954113]];
//    [instrumentTokens addObject:[NSNumber numberWithInt:20517634]];
//    [instrumentTokens addObject:[NSNumber numberWithInt:20517890]];
//    [instrumentTokens addObject:[NSNumber numberWithInt:217138949]];
    
    if(delegate2.instrumentToken.count>0)
    {
    
    NSDictionary * subscribeDict=@{@"a": @"subscribe", @"v":delegate2.instrumentToken};
    
    
    NSData *json;
    
    NSError * error;
    
    
    
    
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:subscribeDict])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
        
        // If no errors, let's view the JSON
        if (json != nil && error == nil)
        {
            string1 = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            
            //            delegate2.message=[NSString stringWithFormat:@"%@",str];
            //
            
            NSLog(@"JSON: %@",string1);
            
        }
    }
    
    
    [self.socket send:string1];
    
    


    
   
        NSArray * mode=@[@"full",delegate2.instrumentToken];
    
    subscribeDict=@{@"a": @"mode", @"v": mode};
    
    
    
//     Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:subscribeDict])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
            
            
            // If no errors, let's view the JSON
            if (json != nil && error == nil)
            {
                _message = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    
                //            delegate2.message=[NSString stringWithFormat:@"%@",str];
    
                NSLog(@"JSON: %@",_message);
    
            }
        }
        [self.socket send:_message];
    
    }
    
    
}




- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    
    
    
    
    
    NSLog(@"The websocket received a message: %@",message);
    
    
    
        NSData * data=[NSKeyedArchiver archivedDataWithRootObject:message];
    
    
    
        
        NSLog(@"size of Object: %zd",malloc_size((__bridge const void *)(message)));

        if(data.length > 247)
        {
            
            
             NSLog(@"The websocket received a message: %@",message);
            
            
            id packets = [message subdataWithRange:NSMakeRange(0,2)];
             packetsNumber = CFSwapInt16BigToHost(*(int16_t*)([packets bytes]));
            NSLog(@" number of packets %i",packetsNumber);
            
             int startingPosition = 2;
            
            for( int i =0; i< packetsNumber ; i++)
            {
               
                
                NSData * packetsLengthData = [message subdataWithRange:NSMakeRange(startingPosition,2)];
                 packetsLength = CFSwapInt16BigToHost(*(int16_t*)([packetsLengthData bytes]));
                startingPosition = startingPosition + 2;
                
                id packetQuote = [message subdataWithRange:NSMakeRange(startingPosition,packetsLength)];
                
                
              
                
                id instrumentTokenData = [packetQuote subdataWithRange:NSMakeRange(0, 4)];
                int32_t instrumentTokenValue = CFSwapInt32BigToHost(*(int32_t*)([instrumentTokenData bytes]));
                
                id lastPrice = [packetQuote subdataWithRange:NSMakeRange(4,4)];
                int32_t lastPriceValue = CFSwapInt32BigToHost(*(int32_t*)([lastPrice bytes]));
                
                
                
                
               
                
                id closingPrice = [packetQuote subdataWithRange:NSMakeRange(40,4)];
                int32_t closePriceValue = CFSwapInt32BigToHost(*(int32_t*)([closingPrice bytes]));
               
                
                
               
                
                
//                id marketDepth = [packetQuote subdataWithRange:NSMakeRange(44,120)];
//                int32_t marketDepthValue = CFSwapInt32BigToHost(*(int32_t*)([marketDepth bytes]));
//                
//                NSLog(@"%@",marketDepth);
                
                if([delegate2.exchange isEqualToString:@"CDS"])
                {
                    lastPriceFlaotValue = (float)lastPriceValue/10000000;
                    
                    closePriceFlaotValue = (float)closePriceValue/10000000;
                    
                    NSLog(@"CLOSE %f",closePriceFlaotValue);
                    
                    changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                    
                    changeFloatPercentageValue = (changeFloatValue/closePriceFlaotValue)/100;
                }
                else
                {
                    lastPriceFlaotValue = (float)lastPriceValue/100;
                    
                    closePriceFlaotValue = (float)closePriceValue/100;
                    
                    NSLog(@"CLOSE %f",closePriceFlaotValue);
                    
                    changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                    
                    changeFloatPercentageValue = (changeFloatValue/closePriceFlaotValue)/100;
                }
               
                startingPosition = startingPosition + packetsLength;
                
                
                @autoreleasepool {
                    
                    NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc]init];
                    [tmpDict setValue:[NSString stringWithFormat:@"%d",instrumentTokenValue] forKey:@"InstrumentToken"];
                    
                    NSString *tmpInstrument = [NSString stringWithFormat:@"%d",instrumentTokenValue];
                   
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",closePriceFlaotValue] forKey:@"ClosePriceValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue] forKey:@"LastPriceValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatValue] forKey:@"ChangeValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatPercentageValue] forKey:@"ChangePercentageValue"];
                    NSLog(@"%@",tmpDict);
                    
                    
                    [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];

                    NSLog(@"%@",instrumentTokensDict);

                    
                    
                  //  [shareListArray addObject:tmpDict];
                   
                }
               

            }
            
            if(allKeysList.count>0)
            {
                [allKeysList removeAllObjects];
            }
            allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
            self.marketTableView.delegate=self;
            self.marketTableView.dataSource=self;
            
            [self.marketTableView reloadData];
            
//            NSLog(@"%@",shareListArray);
            
            
            });
            
                
              //  [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
            
            [self.activityIndicator stopAnimating];
                self.activityIndicator.hidden=YES;
            
            if(allKeysList.count>0)
            {
                self.ifEmptyView.hidden=YES;
                self.marketTableView.hidden=NO;
            }else
            {
                self.ifEmptyView.hidden=NO;
                self.marketTableView.hidden=YES;
            }
        
            

            
        }


    }


- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"The websocket handshake/connection failed with an error: %@", error);
}
- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data
{
     NSLog(@"The websocket received a message: %@", data);
}
- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

      // TableView Delegate and  Data Sources//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   // return [filteredArray count];
    return [allKeysList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
     MarketTableViewCell * cell = [self.marketTableView dequeueReusableCellWithIdentifier:@"Cell"forIndexPath:indexPath];
    
    NSString * str=@"%";
    
    
    
    
    
    NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList objectAtIndex:indexPath.row]]];
    NSString * changeStr=[tmp valueForKey:@"ChangeValue"];
    NSString * changeStrPer=[tmp valueForKey:@"ChangePercentageValue"];
    
    if([changeStr containsString:@"-"])
    {
         cell.percentLbl.text = [tmp valueForKey:@"ChangeValue"];
//        cell.percentLbl.textColor=[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1];
    }
    else{
        cell.percentLbl.text = [tmp valueForKey:@"ChangeValue"];
//        cell.percentLbl.textColor=[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1];
    }
    
    if([changeStrPer containsString:@"-"])
    {
        cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",[tmp valueForKey:@"ChangePercentageValue"],str];
        cell.changePerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
    }
    else{
        cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",[tmp valueForKey:@"ChangePercentageValue"],str];
        cell.changePerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
    }
    
   
    
   
    if(cell.priceLbl.text.length>0)
    {
        
        //    NSNumberFormatter *formatString = [[NSNumberFormatter alloc] init];
        //    NSString * previousValue = cell.priceLbl.text;
        //    NSNumber *number2 = [formatString numberFromString:previousValue];
        
        
        
        CGFloat number2 = (CGFloat)[cell.priceLbl.text floatValue];
        NSLog(@"number2 %f",number2);
        
        NSString * string=[tmp valueForKey:@"LastPriceValue"];
        
        CGFloat number1=(CGFloat)[string floatValue];
        
        NSLog(@"number1 %f",number1);
        
        if(number1<number2)
        {
            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.34];
        }
        
        else if(number1>number2)
        {
            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.34];
        }
        
    }
    
    
    cell.priceLbl.text =[tmp valueForKey:@"LastPriceValue"];
    
    
   
    
    for (int i=0 ;i < delegate2.filteredCompanyName.count ; i++)
    {
        
        NSString *str_ServerToken = [NSString stringWithFormat:@"%@",[[delegate2.filteredCompanyName objectAtIndex:i]valueForKey:@"instrument_token"]];
        NSString *str_KiteToken = [NSString stringWithFormat:@"%@",[tmp valueForKey:@"InstrumentToken"]];
        
        NSLog(@"%@",delegate2.filteredCompanyName);
        
        if([str_ServerToken isEqualToString:str_KiteToken])
        {
            cell.companyName.text= [NSString stringWithFormat:@"%@",[[delegate2.filteredCompanyName objectAtIndex:i]valueForKey:@"symbol"]];
            
            cell.exchangeLbl.text= [NSString stringWithFormat:@"%@",[[delegate2.filteredCompanyName objectAtIndex:i]valueForKey:@"segment"]];
            break;
        }
        
    }
    
    //[cell.tradeBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList objectAtIndex:indexPath.row]]];
    delegate2.instrumentDepthStr=[tmp valueForKey:@"InstrumentToken"];
//    delegate2.symbolDepthStr=[[delegate2.filteredCompanyName objectAtIndex:indexPath.row]valueForKey:@"symbol"];
    NSLog(@"%@",tmp);
   
    
    MarketTableViewCell * cell = [self.marketTableView cellForRowAtIndexPath:indexPath];

    
    
    delegate2.symbolDepthStr=cell.companyName.text;
    
     NSLog(@"%@",delegate2.symbolDepthStr);
    

    
    
   
}


- (IBAction)editTableViewBtn:(id)sender
  {
      
      [self.marketTableView setEditing: YES animated: YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove the row from data model
    [self.companyArray removeAllObjects];
    [self.socket close];
}

-(void)yourButtonClicked:(UIButton*)sender

{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.marketTableView];
    
    
    
    NSIndexPath *indexPath = [self.marketTableView indexPathForRowAtPoint:buttonPosition];
    MarketTableViewCell *cell = [self.marketTableView cellForRowAtIndexPath:indexPath];
    
    NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList objectAtIndex:indexPath.row]]];
    delegate2.instrumentDepthStr=[tmp valueForKey:@"InstrumentToken"];
    
    delegate2.symbolDepthStr=cell.companyName.text;
    
    NSLog(@"%@",delegate2.symbolDepthStr);
    
   


    
    
}

//- (void)viewWillDisappear:(BOOL)animated
//{
//    [self.socket close];
//}


@end
