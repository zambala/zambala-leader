//
//  NotificationTonesPopup.h
//  betazambalaleader
//
//  Created by zenwise mac 2 on 3/23/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTonesPopup : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *noneButton;
@property (strong, nonatomic) IBOutlet UIButton *tone1Button;
@property (strong, nonatomic) IBOutlet UIButton *tone2Button;
@property (strong, nonatomic) IBOutlet UIButton *tone3Button;
@property (strong, nonatomic) IBOutlet UIButton *tone4button;
@property (strong, nonatomic) IBOutlet UIButton *tone5Button;
- (IBAction)onCancelButtonTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
- (IBAction)okButtonTap:(id)sender;

@end
