//
//  ViewController.h
//  OfferPage
//
//  Created by guna on 03/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersPage : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *pageView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *offerViewHgtConstant;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)onBackButtonTap:(id)sender;

@end

