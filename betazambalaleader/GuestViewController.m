//
//  GuestViewController.m
//  testing
//
//  Created by zenwise technologies on 22/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "GuestViewController.h"
#import "AppDelegate.h"
#import "OTPVerificationViewViewController.h"

@interface GuestViewController ()
{
    AppDelegate * delegate1;
    
}

@end

@implementation GuestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
//    [self outFocusTextField];
    
    self.numberTxtField.keyboardType=UIKeyboardTypePhonePad;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)dismissKeyboard
{
    [self.numberTxtField resignFirstResponder];
}

//-(void)inFocusTextField
//{
//    self.numberTxtField.leftViewMode = UITextFieldViewModeAlways;
//    self.numberTxtField.font=[UIFont fontWithName:@"OpenSans" size:13];
//    self.numberTxtField.textColor = [UIColor blackColor];
//    self.numberTxtField.backgroundColor = [UIColor clearColor];
//    
//    CALayer *bottomBorder = [CALayer layer];
//    bottomBorder.frame = CGRectMake(0.0f, self.numberTxtField.frame.size.height - 1, self.numberTxtField.frame.size.width, 1.0f);
//    bottomBorder.backgroundColor = [UIColor redColor].CGColor;
//    [self.numberTxtField.layer addSublayer:bottomBorder];
//}
//
//-(void)outFocusTextField
//{
//    self.numberTxtField.leftViewMode = UITextFieldViewModeAlways;
//    self.numberTxtField.font=[UIFont fontWithName:@"OpenSans" size:13];
//    self.numberTxtField.textColor = [UIColor blackColor];
//    self.numberTxtField.backgroundColor = [UIColor clearColor];
//    
//    CALayer *bottomBorder = [CALayer layer];
//    bottomBorder.frame = CGRectMake(0.0f, self.numberTxtField.frame.size.height - 1, self.numberTxtField.frame.size.width, 1.0f);
//    bottomBorder.backgroundColor = [UIColor greenColor].CGColor;
//    [self.numberTxtField.layer addSublayer:bottomBorder];
//}


//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//
//{
//    [self inFocusTextField];
//    return YES;
//}
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [self outFocusTextField];
//    return YES;
//}



- (IBAction)sendAction:(id)sender {
    
    if(_numberTxtField.text.length==10)
    {
        
        delegate1.phoneNumber= self.numberTxtField.text;
        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                    };
//        NSData *postData = [[NSData alloc] initWithData:[@"{}" dataUsingEncoding:NSUTF8StringEncoding]];
        NSString * url = [NSString stringWithFormat:@"http://2factor.in/API/V1/13ffd06a-2fe3-11e7-8473-00163ef91450/SMS/%@/AUTOGEN",self.numberTxtField.text];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            self.responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            NSLog(@"Response Dict:%@",self.responseDict);
                                                            
                                                            delegate1.phoneNumberSessionID= [self.responseDict objectForKey:@"Details"];
                                                            NSLog(@"Session id:%@",delegate1.phoneNumberSessionID);
                                                        }
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            if([[self.responseDict objectForKey:@"Status"]isEqualToString:@"Success"])
                                                            {
                                                                OTPVerificationViewViewController * otp=[self.storyboard instantiateViewControllerWithIdentifier:@"OTPVerificationViewViewController"];
                                                                [self presentViewController:otp animated:YES completion:nil];
                                                            }
                                                            else
                                                            {
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Something went wrong." preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                }];
                                                                
                                                                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
                                                                
                                                                [alert addAction:okAction];
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        });
                                                        

                                                        
                                                    }];
        [dataTask resume];
    }
        
        
        
        
        

        
    
    else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Number does not match" message:@"Please enter the mobile number that you’ve registered with us." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        

    }
}
@end
