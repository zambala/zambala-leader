//
//  ThemePopUp.m
//  betazambalaleader
//
//  Created by zenwise mac 2 on 3/27/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "ThemePopUp.h"
#import "UIViewController+MJPopupViewController.h"
#import "ProfileSettings.h"



@interface ThemePopUp ()

@end

@implementation ThemePopUp

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lightButton.selected=YES;
    [self.lightButton setImage:[UIImage imageNamed:@"radioSelected.png"] forState:UIControlStateSelected];
    [self.lightButton setImage:[UIImage imageNamed:@"radioUnselect.png"] forState:UIControlStateNormal];
    [self.darkButton setImage:[UIImage imageNamed:@"radioSelected.png"] forState:UIControlStateSelected];
    [self.darkButton setImage:[UIImage imageNamed:@"radioUnselect.png"] forState:UIControlStateNormal];
    
    [self.lightButton addTarget:self action:@selector(onlightButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.darkButton addTarget:self action:@selector(onDarkButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view from its nib.
}

-(void)onlightButtonTap
{
    self.darkButton.selected=NO;
    self.lightButton.selected=YES;
    
}
-(void)onDarkButtonTap
{
    self.lightButton.selected=NO;
    self.darkButton.selected=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onCancelButtonTap:(id)sender {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

}

- (IBAction)onOkButtonTap:(id)sender {
    
    
    if(self.darkButton.selected==YES)
    {
        ProfileSettings * profSettings=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileSettings"];
        [profSettings.themeButton setTitle:@"Dark" forState:UIControlStateNormal];
         [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }else if (self.lightButton.selected==YES)
    {
        ProfileSettings * profSettings=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileSettings"];
        [profSettings.themeButton setTitle:@"Light" forState:UIControlStateNormal];
         [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
}
@end
