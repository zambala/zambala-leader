//
//  PopUpView.h
//  zambala leader
//
//  Created by zenwise mac 2 on 1/4/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSWebSocket.h"
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]


@interface PopUpView : UIViewController<NSURLSessionDelegate>
{
    BOOL checkBoxFlag;
    
    
}
@property NSMutableDictionary * companyNameDict;
@property (strong, nonatomic) IBOutlet UILabel *chngLbl;

@property (strong, nonatomic) IBOutlet UILabel *ltpLbl;
@property (strong, nonatomic) IBOutlet UIButton *chkBoxBtn, *cancelBtn, *postBtn;

@property (strong, nonatomic) IBOutlet UIImageView *premiumImg, *img1, *img2;


@property (copy, readwrite) NSString *symbolStr,*targetPriceStr, *entryPriceStr, *stopLossStr, *premiumStr, *chkBoxStr;

@property (strong, nonatomic) IBOutlet UILabel *symbolLbl, *targetLbl, *entryLbl, *stopLossLbl, *messageLbl, *premiumLbl, *lbl1, *lbl2;

@property NSNumber * premiumAdvice,*messageStr,*duration;

@property NSString *messageStr1;

@property NSString * responseStr;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) PSWebSocket *socket;
@property NSString * message;
@property NSMutableDictionary *symbolNameDict;
@end
